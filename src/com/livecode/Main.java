package com.livecode;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        // initial hash map
        Map<String, String> map = new HashMap<>();
        map.put("Key", "Value");
        map.put("Hello", "World");
        map.put("Welcome", "To Live Code");

        // #1 iterating map by using Map.Entry
        System.out.println("====== By using Map.Entry ======");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        // #2 Iterating by using forEach
        System.out.println("====== By using forEach ======");
        map.forEach((key, value) -> System.out.println(key + " " + value));

        // #3 Iterating by using stream parallel
        System.out.println("====== By using stream ======");
        map.keySet().stream().parallel()
                .forEach(key -> System.out.println(key + " " + map.get(key)));



    }

}
