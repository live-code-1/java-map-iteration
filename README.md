# Java Iterate Over Map | How to Iterate Over MAP in Java? (3 WAYS)

[![Java Iterate Over Map | How to Iterate Over MAP in Java? (3 WAYS)](http://img.youtube.com/vi/sdffbogknJ0/0.jpg)](https://youtu.be/sdffbogknJ0)

*__Kindly Like, Subscribe and Share the YouTube Video__*

## How to Iterate Over MAP in Java?

0. Initialize HasMap
```
Map<String, String> map = new HashMap<>();
map.put("Welcome", "To Live Code");
map.put("key", "value");
```

1. Map.Entry
```
for (Map.Entry<String, String> entry : map.entrySet()) {
    System.out.println(entry.getKey() + " " + entry.getValue());
}
```
2. ForEach
```
map.forEach((key, value) -> System.out.println(key + " " + value));
```
3. Stream (Parallel)
```
map.keySet().stream().parallel()
        .forEach(key -> System.out.println(key + " " + map.get(key)));
```

*Feel free to comment and let us know what kind video you would like to watch*